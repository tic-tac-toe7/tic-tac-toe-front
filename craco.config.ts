import * as path from 'path';

const cracoConfig = {
  webpack: {
    alias: {
      '@themeComponent': path.resolve(__dirname, 'src', 'components', 'theme'),
      '@config': path.resolve(__dirname, 'src', 'config.ts'),
      '@services': path.resolve(__dirname, 'src', 'services'),
      '@contexts': path.resolve(__dirname, 'src', 'contexts'),
      '@enum': path.resolve(__dirname, 'src', 'utils', 'enum'),
      '@helpers': path.resolve(__dirname, 'src', 'utils', 'helpers'),
      '@consts': path.resolve(__dirname, 'src', 'utils', 'consts'),
      '@styles': path.resolve(__dirname, 'src', 'styles')
    }
  }
};

export default cracoConfig;
