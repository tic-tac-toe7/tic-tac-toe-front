import { FC, PropsWithChildren, useState } from 'react';
import { SquareStateEnum } from 'utils/enums';
import GameContext from './game.context';

const GameContextProvider: FC<PropsWithChildren> = ({ children }) => {
  const [isInRoom, setInRoom] = useState(false);
  const [playerSymbol, setPlayerSymbol] = useState<SquareStateEnum>(SquareStateEnum.Cross);
  const [isPlayerTurn, setPlayerTurn] = useState(false);
  const [isGameStarted, setIsGameStarted] = useState(false);
  const [playersCount, setPlayersCount] = useState({ currentPlayer: 0, otherPlayer: 0 });

  const value = {
    isInRoom,
    setInRoom,
    playerSymbol,
    setPlayerSymbol,
    isPlayerTurn,
    setPlayerTurn,
    isGameStarted,
    setIsGameStarted,
    playersCount,
    setPlayersCount
  };
  return <GameContext.Provider value={value}>{children}</GameContext.Provider>;
};

export default GameContextProvider;
