import { SquareStateEnum } from 'utils/enums';

export interface GameContextInterface {
  isInRoom: boolean;
  setInRoom: (inRoom: boolean) => void;
  playerSymbol: SquareStateEnum;
  setPlayerSymbol: (playerSymbol: SquareStateEnum) => void;
  isPlayerTurn: boolean;
  setPlayerTurn: (isPlayerTurn: boolean) => void;
  isGameStarted: boolean;
  setIsGameStarted: (isGameStarted: boolean) => void;
  playersCount: { currentPlayer: number; otherPlayer: number };
  setPlayersCount: (playersCount: { currentPlayer: number; otherPlayer: number }) => void;
}
