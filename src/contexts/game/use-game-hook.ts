import { useContext } from 'react';
import gameContext from './game.context';
import { GameContextInterface } from './game.context.interface';

export const useGameHook = () => {
  const context = useContext<GameContextInterface>(gameContext);
  if (context === undefined) {
    throw new Error(
      'GameContext was not provided. Make sure your component is a part of GameProvider'
    );
  }

  return context;
};
