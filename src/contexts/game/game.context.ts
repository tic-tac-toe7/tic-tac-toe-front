import { createContext } from 'react';
import { SquareStateEnum } from 'utils/enums';
import { GameContextInterface } from './game.context.interface';

const gameContext = createContext<GameContextInterface>({
  isInRoom: false,
  setInRoom: () => {},
  playerSymbol: SquareStateEnum.Cross,
  setPlayerSymbol: () => {},
  isPlayerTurn: false,
  setPlayerTurn: () => {},
  isGameStarted: false,
  setIsGameStarted: () => {},
  playersCount: { currentPlayer: 0, otherPlayer: 0 },
  setPlayersCount: () => {}
} as GameContextInterface);

export default gameContext;
