import { useGameHook } from '@contexts/game/use-game-hook';
import { ChangeEvent, FC, FormEvent, useState } from 'react';
import socketService from '@services/socket';
import './JoinRoom.scss';
import gameService from '@services/game';

const JoinRoom: FC = () => {
  const [roomName, setRoomName] = useState('');
  const [isJoining, setIsJoining] = useState(false);
  const { setInRoom } = useGameHook();

  const handleRoomChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    setRoomName(value);
  };
  const joinRoom = async (e: FormEvent) => {
    e.preventDefault();
    const socket = socketService.socket;
    if (!roomName || roomName.trim() === '' || !socket) return;

    setIsJoining(true);
    const joined = await gameService.joinGameRoom(socket, roomName).catch((err) => alert(err));
    if (joined) {
      setInRoom(true);
    }
    setIsJoining(false);
  };

  return (
    <div className="join-room">
      <form onSubmit={joinRoom}>
        <h4>Enter room id to join the game </h4>
        <input
          onChange={handleRoomChange}
          type="text"
          className="room-id-input"
          value={roomName}
          placeholder="Room ID"
        />
        <button type="submit" className="join-button" disabled={isJoining}>
          {isJoining ? 'Joining...' : 'Joing'}
        </button>
      </form>
    </div>
  );
};

export default JoinRoom;
