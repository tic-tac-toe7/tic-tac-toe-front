import './Game.scss';
import { FC, useEffect, useState } from 'react';
import { SquareStateEnum, WinningStatus } from 'utils/enums';
import Circle from './Circle/Circle';
import Cross from './Cross/Cross';
import cn from 'clsx';
import { useGameHook } from '@contexts/game/use-game-hook';
import gameService from '@services/game';
import socketService from '@services/socket';

export type PlayMatrixType = Array<Array<SquareStateEnum | null>>;
export interface StartGameInterface {
  start: boolean;
  symbol: SquareStateEnum;
}

const Game: FC = () => {
  const [matrix, setMatrix] = useState<PlayMatrixType>([
    [null, null, null],
    [null, null, null],
    [null, null, null]
  ]);
  const timer = 15;
  const [counter, setCounter] = useState(timer);
  const {
    playerSymbol,
    isPlayerTurn,
    setPlayerTurn,
    setPlayerSymbol,
    setIsGameStarted,
    isGameStarted,
    setPlayersCount,
    playersCount
  } = useGameHook();

  const [rounds, setRounds] = useState<number[]>([]);

  const checkGameState = (matrix1: PlayMatrixType) => {
    for (let i = 0; i < matrix1.length; i++) {
      const row = [];
      for (let j = 0; j < matrix1[i].length; j++) {
        row.push(matrix1[i][j]);
      }

      if (row.every((value) => value && value === playerSymbol)) {
        return [true, false];
      } else if (row.every((value) => value && value !== playerSymbol)) {
        return [true, false];
      }
    }
    for (let i = 0; i < matrix1.length; i++) {
      const column = [];
      for (let j = 0; j < matrix1[i].length; j++) {
        column.push(matrix1[j][i]);
      }

      if (column.every((value) => value && value === playerSymbol)) {
        return [true, false];
      } else if (column.every((value) => value && value !== playerSymbol)) {
        return [true, false];
      }
    }

    if (matrix1[1][1]) {
      if (matrix1[0][0] === matrix1[1][1] && matrix1[2][2] === matrix1[1][1]) {
        if (matrix1[1][1] === playerSymbol) return [true, false];
        else return [false, true];
      }

      if (matrix1[2][0] === matrix1[1][1] && matrix1[0][2] === matrix1[1][1]) {
        if (matrix1[1][1] === playerSymbol) return [true, false];
        else return [false, true];
      }
    }

    if (matrix1.every((m) => m.every((v) => v !== null))) {
      return [true, true];
    }

    return [false, false];
  };

  const updateGameMatrix = (column: number, row: number, symbol: SquareStateEnum) => {
    const newMatrix = [...matrix];
    if (newMatrix[row][column] === null) {
      newMatrix[row][column] = symbol;
      setMatrix(newMatrix);

      if (socketService.socket) {
        gameService.updateGame(socketService.socket, newMatrix);
        const [currentPlayerWon, otherPlayerWon] = checkGameState(newMatrix);
        if (currentPlayerWon && otherPlayerWon) {
          gameService.gameWin(socketService.socket, {
            currentPlayer: playersCount.currentPlayer + 1,
            otherPlayer: playersCount.otherPlayer + 1,
            status: WinningStatus.Tie
          });
        } else if (currentPlayerWon && !otherPlayerWon) {
          gameService.gameWin(socketService.socket, {
            currentPlayer: playersCount.currentPlayer + 1,
            otherPlayer: playersCount.otherPlayer,
            status: WinningStatus.Won
          });
        }

        setPlayerTurn(false);
      }
    }
  };

  const handleGameUpdate = () => {
    if (socketService.socket)
      gameService.onGameUpdate(socketService.socket, (newMatrix) => {
        setMatrix(newMatrix);
        setPlayerTurn(true);
        setCounter(timer);
        // checkGameState(newMatrix);
      });
  };

  const handleGameStart = () => {
    const socket = socketService.socket;

    if (socket) {
      gameService.startGame(socket).catch((err) => alert(err));
      gameService.onStartGame(socket, (options) => {
        setIsGameStarted(true);
        setPlayerSymbol(options.symbol);
        if (options.start) {
          setPlayerTurn(options.start);
        } else {
          setPlayerTurn(false);
        }
      });
    }
  };

  const handleGameWin = () => {
    if (socketService.socket) {
      gameService.onGameWin(socketService.socket, (values) => {
        if (values.status !== WinningStatus.Tie) {
          if (values.status === WinningStatus.Won) {
            setPlayerTurn(true);
            setCounter(timer);
          } else {
            setPlayerTurn(false);
          }
        }

        if (values.currentPlayer === 10 || values.otherPlayer === 10) {
          setPlayerTurn(false);
        }
        setPlayersCount({ currentPlayer: values.currentPlayer, otherPlayer: values.otherPlayer });
        setMatrix([
          [null, null, null],
          [null, null, null],
          [null, null, null]
        ]);
      });
    }
  };

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (counter > 0 && isGameStarted && isPlayerTurn) {
      timeout = setTimeout(() => setCounter(counter - 1), 1000);
    }

    return () => clearTimeout(timeout);
  }, [counter, isGameStarted, isPlayerTurn]);

  useEffect(() => {
    if (socketService.socket && counter === 0) {
      gameService.gameWin(socketService.socket, {
        currentPlayer: playersCount.currentPlayer,
        otherPlayer: playersCount.otherPlayer + 1,
        status: WinningStatus.Lost
      });
    }
  }, [counter]);

  useEffect(() => {
    handleGameUpdate();
    handleGameStart();
    handleGameWin();
  }, []);

  useEffect(() => {
    if (playersCount.currentPlayer !== 10 && playersCount.otherPlayer !== 10) {
      const p = [...rounds];
      if (p.includes(playersCount.currentPlayer)) {
        p.push(0);
      } else {
        p.push(playersCount.currentPlayer);
      }

      for (let i = 0; i < p.length; i++) {
        if (p[i] !== 0 && p[i - 1] !== 0 && p[i - 2] !== 0 && socketService.socket) {
          gameService.gameWin(socketService.socket, {
            currentPlayer: 10,
            otherPlayer: playersCount.otherPlayer,
            status: WinningStatus.Won
          });
        }
      }
      setRounds(p);
    }
  }, [playersCount]);

  return (
    <div className="game">
      {!isGameStarted && <h2>Waiting for other player to join to start the game</h2>}
      {(!isGameStarted || !isPlayerTurn) && <div className="play-stoper"></div>}

      {playersCount.currentPlayer === 10 && <h3>You won!</h3>}
      {playersCount.otherPlayer === 10 && <h3>You lost!</h3>}
      <div className="game__info">
        <div>
          <div>Your count: {playersCount.currentPlayer}</div>
          <div>Other player count: {playersCount.otherPlayer}</div>
        </div>
        {playersCount.currentPlayer !== 10 && playersCount.otherPlayer !== 10 && (
          <div>
            <div>{isPlayerTurn ? "It's your turn" : "It's your opponent's turn"}</div>
            {isGameStarted && <div>Left time to push: {counter}</div>}
          </div>
        )}
      </div>

      {matrix.map((row, rowId) => {
        return (
          <div className="row" key={rowId}>
            {row.map((column, columnId) => {
              return (
                <div
                  onClick={() => updateGameMatrix(columnId, rowId, playerSymbol)}
                  className={cn('cell', {
                    ['border-right']: columnId < 2,
                    ['border-left']: columnId > 0,
                    ['border-bottom']: rowId > 2,
                    ['border-top']: rowId > 0
                  })}
                  key={columnId}>
                  {column ? column === SquareStateEnum.Cross ? <Cross /> : <Circle /> : null}
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default Game;
