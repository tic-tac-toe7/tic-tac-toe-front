export const NODE_ENV = process.env.NODE_ENV || 'development';
export const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:9000';
