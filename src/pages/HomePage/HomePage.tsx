import { useGameHook } from '@contexts/game/use-game-hook';
import Game from 'components/Game/Game';
import JoinRoom from 'components/JoinRoom/JoinRoom';

import './HomePage.scss';
const HomePage = () => {
  const { isInRoom } = useGameHook();

  return (
    <div className="home">
      <div className="home__title">
        <h1>Tick Tack Toe Game</h1>
      </div>
      {!isInRoom && <JoinRoom />}
      {isInRoom && <Game />}
    </div>
  );
};

export default HomePage;
