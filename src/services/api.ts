/* eslint no-console: 0 */
import { API_URL, NODE_ENV } from '@config';
import { routes } from '@consts/route.consts';
import axios, { AxiosResponse } from 'axios';

const axiosInstance = axios.create({
  baseURL: API_URL,
  withCredentials: true,
  headers: {
    'Content-type': 'application/json'
  }
});

axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => response.data,
  async (e) => {
    let status = 200;

    const { config: originalConfig } = e;

    if (e.response?.status) status = e.response.status;

    let message = '';
    if (e.request.response) {
      try {
        const d = JSON.parse(e.request.response);
        message = d.message;
        try {
          if (d.data?.length) {
            message += 'Errors ';
            d.data.forEach(
              ({ constraints }: { constraints: { [key: string]: string } }, i: number) => {
                message += `${i === 0 ? '' : '. '}${Object.values(constraints)[0]}`;
              }
            );
          }
        } catch {
          if (NODE_ENV !== 'production') console.error("can't parse response");
        }
        d.message = message;
        if (
          status === 401 &&
          (message === 'Unauthorized' || message === 'Bad token') &&
          !originalConfig._retry
        ) {
          const { pathname, search, hash } = window.location;
          let redirect = '';
          if (pathname !== routes.home) {
            originalConfig._retry = true;
            try {
              await axiosInstance.post(`auth/refresh-token`);
              return await axiosInstance(originalConfig);
            } catch (_error) {
              redirect = [pathname, search, hash].join('');
              window.location.href = `${routes.home}?redirect=${redirect}`;
              return Promise.reject(_error);
            }
          }
        }
        return await Promise.reject(d);
      } catch (e1) {
        if (NODE_ENV !== 'production') console.error(e1);
        return Promise.reject(new Error((e1 as Error).message));
      }
    }

    try {
      return await Promise.reject(JSON.parse(String(e.request.response)));
    } catch {
      return Promise.reject(new Error('Internal server error'));
    }
  }
);

export default axiosInstance;
