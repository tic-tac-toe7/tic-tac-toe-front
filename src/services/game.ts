import { PlayMatrixType, StartGameInterface } from 'components/Game/Game';
import { Socket } from 'socket.io-client';
import { WinningStatus } from 'utils/enums';

class GameService {
  public async joinGameRoom(socket: Socket, roomId: string): Promise<boolean> {
    return new Promise((rs, rj) => {
      socket.emit('join_game', { roomId });
      socket.on('room_joined', () => rs(true));
      socket.on('room_join_error', ({ error }) => rj(error));
    });
  }

  public async updateGame(socket: Socket, gameMatix: PlayMatrixType) {
    socket.emit('update_game', { matrix: gameMatix });
  }

  public async onGameUpdate(socket: Socket, listener: (matrix: PlayMatrixType) => void) {
    socket.on('on_game_update', ({ matrix }) => listener(matrix));
  }

  public async startGame(socket: Socket) {
    socket.emit('start_game');
  }

  public async onStartGame(socket: Socket, listener: (options: StartGameInterface) => void) {
    socket.on('on_start_game', listener);
  }

  public async gameWin(
    socket: Socket,
    message: { currentPlayer: number; otherPlayer: number; status: WinningStatus }
  ) {
    socket.emit('game_win', message);
  }

  public async onGameWin(
    socket: Socket,
    listener: (values: {
      currentPlayer: number;
      otherPlayer: number;
      status: WinningStatus;
    }) => void
  ) {
    socket.on('on_game_win', listener);
  }
}

export default new GameService();
