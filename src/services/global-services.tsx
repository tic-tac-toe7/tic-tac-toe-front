import { FC, PropsWithChildren } from 'react';

const GlobalServices: FC<PropsWithChildren> = ({ children }) => {
  return <>{children}</>;
};

export default GlobalServices;
