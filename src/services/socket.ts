import { io, Socket } from 'socket.io-client';

class SocketService {
  public socket: Socket | null = null;

  public connect(url: string): Promise<Socket> {
    return new Promise((rs, rj) => {
      this.socket = io(url);

      if (!this.socket) return rj();

      this.socket?.on('connect', () => {
        rs(this.socket as Socket);
      });

      this.socket.on('connect error', (error) => {
        console.log('Connection error: ', error);
        rj(error);
      });
    });
  }
}

export default new SocketService();
