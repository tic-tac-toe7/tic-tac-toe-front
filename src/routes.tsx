import HomePage from 'pages/HomePage/HomePage';
import { RouteObject, useRoutes } from 'react-router-dom';
import { routes as routesConst } from 'utils/consts/route.consts';

export const routes: RouteObject[] = [
  {
    path: routesConst.home,
    element: <HomePage />
  }
];

export default function Router() {
  return useRoutes(routes);
}
