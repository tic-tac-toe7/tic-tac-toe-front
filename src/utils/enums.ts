export enum SquareStateEnum {
  Circle = 'circle',
  Cross = 'cross'
}

export enum WinningStatus {
  Tie = 'tie',
  Won = 'won',
  Lost = 'lost'
}
