import { BrowserRouter } from 'react-router-dom';
import Router from './routes';
import '@styles/global.scss';
import socketService from '@services/socket';
import { API_URL } from '@config';
import { useEffect } from 'react';
import GameContextProvider from '@contexts/game/game.provider';

function App() {
  const connectSocket = async () => {
    await socketService.connect(API_URL).catch((err) => {
      console.log('Error: ', err);
    });
  };

  useEffect(() => {
    connectSocket();
  }, []);

  return (
    <BrowserRouter>
      <GameContextProvider>
        <Router />
      </GameContextProvider>
    </BrowserRouter>
  );
}

export default App;
